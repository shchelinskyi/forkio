const hamMenu = document.querySelector('.ham-menu');
const hamMenuContent = document.querySelector('.ham-menu-content');

const toggleMenu = () => {
    hamMenu.classList.toggle('active');
    hamMenuContent.classList.toggle("show");
}

window.addEventListener('resize', () => {
        hamMenu.classList.remove('active');
        hamMenuContent.classList.remove("show");
});

document.addEventListener('click', (e) => {
    const target = e.target;
    const its_menu = target === hamMenuContent || hamMenuContent.contains(target);
    const menu_is_active = hamMenuContent.classList.contains("show");

    if (!its_menu && e.target !== hamMenu && menu_is_active ) {
        toggleMenu();
    }
})

hamMenu.addEventListener('click', () => {
   toggleMenu();
})

